from fastapi.testclient import TestClient

from toutafrique_api import create_app


def test_create_app(settings):
    app = create_app(settings)
    with TestClient(app):
        pass


def test_create_app_with_redis(settings_with_redis_url, redis_service):
    app = create_app(settings_with_redis_url)
    with TestClient(app):
        pass


def test_cors_middleware(client):
    response = client.get(
        "/",
        headers={"Origin": "http://testclient"},
    )
    assert response.status_code == 200
    assert response.headers["access-control-allow-origin"] == "*"
    assert response.headers["access-control-allow-credentials"] == "true"


def test_cors_middleware_with_redis(client_with_redis):
    # Check that enabling the cache does not lead to skip CORS headers.
    for attempt in range(2):
        response = client_with_redis.get(
            "/",
            headers={"Origin": f"http://testclient_{attempt}"},
        )
        assert response.status_code == 200
        assert response.headers["access-control-allow-origin"] == "*"
        assert response.headers["access-control-allow-credentials"] == "true"
