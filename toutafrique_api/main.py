# toutafrique-api: Africartes Web API
# By: Jailbreak Team <team@jailbreak.paris>
#
# Copyright (C) 2020 Jailbreak
# https://gitlab.com/africartes/africartes-api
#
# toutafrique-api is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# toutafrique-api is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""Init API server."""


import logging
from typing import Optional

import aioredis
import fastapi_cache.decorator
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.middleware.gzip import GZipMiddleware
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from fs.base import FS
from redis import Redis

from .adapters.redis_cache import RedisCache
from .cache import Cache
from .config import Settings
from .filesystem import FileSystemError, load_data_fs
from .redis import load_redis_client
from .router import create_router
from .version import __version__

logger = logging.getLogger(__name__)


def create_app(settings: Optional[Settings] = None):
    settings = init_settings(settings)

    logging.basicConfig(level=settings.GLOBAL_LOG_LEVEL)
    logger.setLevel(settings.APP_LOG_LEVEL)

    app = FastAPI(
        description="Serve Africartes data",
        docs_url=settings.DOCS_URL,
        root_path=settings.API_BASE_PATH,
        title="toutafrique-api",
        version=__version__,
    )

    app.state.settings = settings

    init_middlewares(app, settings)

    @app.on_event("startup")
    async def startup_event():
        """Initialize external resources now, to fail fast at application startup."""

        if settings.REDIS_URL is None:
            logger.info("REDIS_URL is not set, skipping cache initialization")
            file_cache = None
            cache_endpoint_decorator = lambda x: x  # noqa
        else:
            logger.info(f"REDIS_URL={settings.REDIS_URL!r}, initializing cache...")

            # Initializing file cache
            redis_client = init_redis_client(settings)
            file_cache = init_file_cache(settings, redis_client)

            # Initializing route cache
            aioredis_client = await init_aioredis_client(settings)
            init_route_cache(settings, aioredis_client)
            cache_endpoint_decorator = fastapi_cache.decorator.cache(
                expire=settings.ROUTE_CACHE_TTL
            )

        data_fs = init_data_fs(settings, file_cache)
        app.state.data_fs = data_fs

        router = create_router(cache_endpoint_decorator)
        app.include_router(router)

    @app.on_event("shutdown")
    def shutdown_event():
        data_fs = app.state.data_fs
        close_data_fs(data_fs)

    return app


def close_data_fs(data_fs: FS):
    """Close data file-system."""
    logger.info(f"Closing data file-system {data_fs!r}...")
    data_fs.close()


async def init_aioredis_client(settings: Settings):
    assert settings.REDIS_URL is not None

    try:
        aioredis_client = await aioredis.create_redis_pool(
            settings.REDIS_URL, encoding="utf8"
        )
    except ConnectionError as exc:
        raise ConnectionError(
            f"Could not connect to Redis server from config: "
            f"REDIS_URL={settings.REDIS_URL!r}"
        ) from exc

    logger.info(f"Async Redis client created: {aioredis_client!r}")
    return aioredis_client


def init_data_fs(settings: Settings, file_cache: Optional[Cache] = None) -> FS:
    logger.info(
        f"Loading data file-system from DATA_FS_URL={settings.DATA_FS_URL!r}..."
    )
    try:
        data_fs = load_data_fs(settings.DATA_FS_URL, file_cache)
    except FileSystemError as exc:
        raise ValueError(
            f"Could not initialize file-system from config: "
            f"DATA_FS_URL={settings.DATA_FS_URL!r}"
        ) from exc
    logger.info(f"Data file-system created: {data_fs!r}")
    return data_fs


def init_file_cache(settings: Settings, redis_client: Redis) -> Cache:
    file_cache = RedisCache(
        redis_client,
        ttl=settings.DATA_FS_CACHE_TTL,
        key_prefix=settings.DATA_FS_CACHE_KEY_PREFIX,
    )
    logger.info(f"File cache created: {file_cache!r}")
    return file_cache


def init_middlewares(app: FastAPI, settings: Settings):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.add_middleware(GZipMiddleware)

    if settings.SENTRY_DSN is not None:
        try:
            import sentry_sdk
            from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
        except ImportError as exc:
            raise ImportError(
                f"SENTRY_DSN={settings.SENTRY_DSN!r} but could not import sentry_sdk"
            ) from exc

        sentry_sdk.init(dsn=settings.SENTRY_DSN)
        app.add_middleware(SentryAsgiMiddleware)


def init_redis_client(settings: Settings) -> Redis:
    assert settings.REDIS_URL is not None

    try:
        redis_client = load_redis_client(settings.REDIS_URL)
    except ConnectionError as exc:
        raise ConnectionError(
            f"Could not connect to Redis server from config: "
            f"REDIS_URL={settings.REDIS_URL!r}"
        ) from exc

    logger.info(f"Redis client created: {redis_client!r}")
    return redis_client


def init_route_cache(settings: Settings, aioredis_client):
    FastAPICache.init(
        RedisBackend(aioredis_client), prefix=settings.ROUTE_CACHE_KEY_PREFIX
    )


def init_settings(initial_settings: Optional[Settings] = None) -> Settings:
    if initial_settings is not None:
        return initial_settings
    return Settings()
