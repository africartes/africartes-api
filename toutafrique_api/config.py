# toutafrique-api: Africartes Web API
# By: Jailbreak Team <team@jailbreak.paris>
#
# Copyright (C) 2020 Jailbreak
# https://gitlab.com/africartes/africartes-api
#
# toutafrique-api is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# toutafrique-api is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""Application configuration"""


from typing import Optional

from pydantic import BaseSettings

default_cache_ttl = 60 * 60


class Settings(BaseSettings):
    # Logger settings
    GLOBAL_LOG_LEVEL: str = "WARNING"
    APP_LOG_LEVEL: str = "WARNING"

    # API settings
    API_BASE_PATH: str = ""
    DOCS_URL: str = "/docs"

    # sentry.io (application monitoring) settings
    SENTRY_DSN: Optional[str] = None

    # Data URL for PyFilesystem
    DATA_FS_URL: str

    # Cache settings
    DATA_FS_CACHE_TTL: int = default_cache_ttl
    DATA_FS_CACHE_KEY_PREFIX: str = "africartes-api:data_fs"

    # Time to live of the route cache, in seconds
    ROUTE_CACHE_TTL: int = default_cache_ttl
    ROUTE_CACHE_KEY_PREFIX: str = "africartes-api:route"

    # Redis is used by the route cache
    REDIS_URL: Optional[str] = None

    class Config:
        env_file = ".env"
