from dataclasses import dataclass
from typing import Generic, TypeVar

from redis import Redis

T = TypeVar("T", bytes, float, int, str)


@dataclass
class RedisCache(Generic[T]):
    redis: Redis
    ttl: int
    key_prefix: str = ""

    def get(self, key: str) -> T:
        k = self._with_prefix(key)
        return self.redis.get(k)

    def set(self, key: str, value: T):
        k = self._with_prefix(key)
        self.redis.set(k, value)
        self.redis.expire(k, self.ttl)

    def _with_prefix(self, key: str) -> str:
        return f"{self.key_prefix}:{key}"
