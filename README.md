# Africartes API

Serve indicators and topologies.

## Installation

This application is configured to be opened in a [dev container](https://code.visualstudio.com/docs/remote/containers).

Otherwise, you can develop in a virtualenv:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
pip install -r requirements-tests.txt
pip install -e .
```

## Configuration

```bash
cp .env.example .env
```

Then adjust the settings.

All available config keys are defined in [`config.py`](toutafrique_api/config.py).

### Source data

The app accesses data from `DATA_FS_URL`.

Thanks to [pyfilesystem](https://docs.pyfilesystem.org/), the API can read its data from any supported filesystem. For example, a local directory or a remote S3 bucket (with the [S3FS plugin](https://fs-s3fs.readthedocs.io/)).

- In production, data is stored in a public S3.
- In development, data is usually stored in a directory, but you can choose to read data from a S3 bucket instead, or any backend supported by pyfilesystem.

You can generate source data from scratch by running [africartes-data-processing](https://gitlab.com/africartes/africartes-data-processing/). The script takes a few minutes to write data.

If you have the credentials, you can synchronise the production S3 bucket to your local machine.

```bash
# Define AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY in .env
pip install awscli
mkdir data
dotenv run aws s3 sync s3://afd-toutafrique-data ./data
```

The `data` directory now contains the latest production data. You can launch this command again to refresh.

In `.env`, update `DATA_FS_URL` to `./data` to let the application know about the data new location.

## Run

If you choosed to work in a dev container, just press F5 in VSCode.

Otherwise:

```bash
./serve.sh
```

Visit http://localhost:8000 in your web browser.

Check also the [API docs](http://127.0.0.1:8000/docs).

## Production

- Container images are built by GitLab CI (cf `.gitlab-ci.yml`).
- Kubernetes Deployment: cf https://gitlab.com/africartes/africartes-ops
