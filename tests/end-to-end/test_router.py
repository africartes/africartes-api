"""Tests for the route handlers of the router."""

import pytest


def test_index(client):
    response = client.get("/")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert response.json()["result"]["message"].startswith(
        "This is the index endpoint of Africartes API. "
        "Its documentation is there: http://testserver/docs"
    )


def test_index_with_api_base_path(client_with_api_base_path):
    response = client_with_api_base_path.get("/")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert response.json()["result"]["message"].startswith(
        "This is the index endpoint of Africartes API. "
        "Its documentation is there: http://testserver/foo/docs"
    )


def test_indicators(client):
    response = client.get("/indicators")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["categories"][0]["slug"] == "eco"
    assert data["result"]["indicators"][0]["slug"] == "pib"


def test_indicators_with_cache(client_with_redis, redis_service):
    response = client_with_redis.get("/indicators")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["categories"][0]["slug"] == "eco"
    assert data["result"]["indicators"][0]["slug"] == "pib"

    # Check file-system cache
    key = "africartes-api:data_fs:indicators_info.json"
    assert redis_service.get(key).startswith(b"{")  # it's a dict

    # Check route cache
    key = "africartes-api:route::toutafrique_api.router:indicators:():{}"
    assert redis_service.get(key).startswith(b"{")  # it's a dict

    response = client_with_redis.get("/indicators")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    assert data["result"]["categories"][0]["slug"] == "eco"
    assert data["result"]["indicators"][0]["slug"] == "pib"


def test_indicators_for_area(client):
    response = client.get("/indicators/area/africa/pib")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["slug"] == "pib"
    assert data["result"]["observations"][0]["Year"] == 1980
    assert data["result"]["observations"][0]["AGO"] == 6.639


def test_indicators_for_area_invalid_area_slug(client):
    response = client.get("/indicators/area/foo/pib")
    assert response.status_code == 422


def test_indicators_for_area_invalid_indicator_slug(client):
    response = client.get("/indicators/area/africa/foo")
    assert response.status_code == 404


def test_indicators_by_territories_continent(client):
    response = client.get("/indicators/territories?code=AF")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["abo_mobile"]["AF"]["period"][0] == 1960
    assert data["result"]["abo_mobile"]["AF"]["value"][0] == 0


def test_indicators_by_territories_country(client):
    response = client.get("/indicators/territories?code=ABW")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["abo_mobile"]["ABW"]["period"][0] == 1992
    assert data["result"]["abo_mobile"]["ABW"]["value"][0] == 0.00002


def test_indicators_by_territories_many_countries(client):
    response = client.get("/indicators/territories?code=AFG&code=ABW")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["abo_mobile"]["ABW"]["period"][0] == 1992
    assert data["result"]["abo_mobile"]["ABW"]["value"][0] == 0.00002
    assert data["result"]["abo_mobile"]["AFG"]["period"][0] == 2008
    assert data["result"]["abo_mobile"]["AFG"]["value"][0] == 7.898909


def test_indicators_by_territories_dr(client):
    response = client.get("/indicators/territories?code=DR_AFA")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["abo_mobile"]["DR_AFA"]["period"][0] == 1995
    assert data["result"]["abo_mobile"]["DR_AFA"]["value"][0] == 0.05424230000000001


def test_indicators_by_territories_no_code(client):
    response = client.get("/indicators/territories")
    assert response.status_code == 422


def test_indicators_by_territories_invalid_code(client):
    response = client.get("/indicators/territories?code=foo")
    assert response.status_code == 404


def test_population_by_country(client):
    response = client.get("/population_by_country/AGO")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["country"] == "AGO"
    assert data["result"]["legend"]["age"][0] == ["Y5T10", "5-10"]
    assert data["result"]["observations"]["1978"][0] == ["F", "Y10T14", 455.905]


def test_population_by_country_invalid_country(client):
    response = client.get("/population_by_country/foo")
    assert response.status_code == 404


def test_territories(client):
    response = client.get("/territories")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["areas"]["africa"][0] == "AGO"
    assert data["result"]["areas"]["africa_dr"][0] == "DR_AFA"
    assert data["result"]["areas"]["world"][0] == "ABW"
    assert data["result"]["territories"][0]["code"] == "ABW"
    assert data["result"]["territories"][0]["type"] == "country"


def test_topologies(client):
    response = client.get("/topologies")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"][0]["slug"] == "africa"
    assert data["result"][1]["slug"] == "africa_dr"
    assert data["result"][2]["slug"] == "world"


@pytest.mark.parametrize("area_slug", ["africa", "africa_dr", "world"])
def test_topology(client, area_slug):
    response = client.get(f"/topologies/{area_slug}")
    assert response.status_code == 200
    assert response.headers["content-type"] == "application/json"
    data = response.json()
    assert data["success"] is True
    assert data["result"]["type"] == "FeatureCollection"
    assert data["result"]["features"][0]["type"] == "Feature"


def test_topology_invalid_area_slug(client):
    response = client.get("/topologies/foo")
    assert response.status_code == 422
