import logging

import redis.exceptions
from redis import Redis

logger = logging.getLogger(__name__)


def load_redis_client(url: str):
    logger.info(f"Creating Redis client from {url!r}...")
    client = Redis.from_url(url)
    try:
        client.ping()
    except redis.exceptions.ConnectionError as exc:
        raise ConnectionError(
            f"Could not connect to Redis server from {url!r}"
        ) from exc
    return client
