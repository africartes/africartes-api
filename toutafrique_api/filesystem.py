# toutafrique-api: Africartes Web API
# By: Jailbreak Team <team@jailbreak.paris>
#
# Copyright (C) 2020 Jailbreak
# https://gitlab.com/africartes/africartes-api
#
# toutafrique-api is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# toutafrique-api is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import logging
from typing import Optional, Text

import fs
import fs.opener.errors
import fs.wrap
from fs.base import FS
from fs.wrapfs import WrapFS

from .cache import Cache

logger = logging.getLogger(__name__)


class FileSystemError(Exception):
    pass


def load_data_fs(url: str, cache: Optional[Cache] = None) -> FS:
    """Load data from agnostic filesystem."""
    try:
        data_fs = fs.open_fs(url)
    except fs.opener.errors.UnsupportedProtocol as exc:
        raise FileSystemError(f"Could not load file-system from URL {url!r}") from exc
    data_fs = fs.wrap.read_only(data_fs)
    if cache:
        data_fs = WrapCache(data_fs, cache)
    return data_fs


class WrapCache(WrapFS):
    """Use Redis as TTL cache.

    Cached methods: readtext.
    """

    def __init__(self, wrap_fs, cache: Cache):
        self.cache = cache
        super().__init__(wrap_fs)

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self._wrap_fs!r}, cache={self.cache!r})"

    def readtext(self, path: Text, *args, **kwargs) -> str:
        doc_bytes = self.cache.get(path)
        if doc_bytes is not None:
            return doc_bytes.decode()

        logger.debug(
            f"[Cache miss] file {path!r} not found in {self.cache!r}, "
            f"loading from {self._wrap_fs!r} then adding it to cache..."
        )
        file_text = super().readtext(path, *args, **kwargs)
        self.cache.set(path, file_text)
        return file_text


def load_json_file(fs: FS, path: str):
    """Load JSON file."""
    doc_str = fs.readtext(path)
    try:
        return json.loads(doc_str)
    except ValueError as exc:
        raise ValueError(f"Could not decode JSON file {path!r} in FS {fs!r}") from exc
