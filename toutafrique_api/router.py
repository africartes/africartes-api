# toutafrique-api: Africartes Web API
# By: Jailbreak Team <team@jailbreak.paris>
#
# Copyright (C) 2020-2021 Jailbreak
# https://gitlab.com/africartes/africartes-api
#
# toutafrique-api is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# toutafrique-api is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""API router and its route handlers."""

from enum import Enum
from io import StringIO
from pathlib import Path
from typing import List
from urllib.parse import urljoin

import fs
import pandas as pd
from fastapi import APIRouter, Query, Request, status

from . import model
from .filesystem import load_json_file
from .model import IndicatorInfoRepository
from .util import (
    build_indicators_for_area,
    build_population_data,
    build_response_functions,
)

# TODO Internalize in repository
INDICATORS_INFO_JSON = "indicators_info.json"

ok_response, err_response = build_response_functions()


class AreaSlug(str, Enum):
    """Slug values for geographical areas."""

    world = "world"
    africa = "africa"
    africa_dr = "africa_dr"


def create_router(cache_endpoint):
    router = APIRouter()

    @router.get("/", include_in_schema=False)
    def index(request: Request):
        """Root endpoint."""
        settings = request.app.state.settings
        docs_url = urljoin(
            str(request.url), f"{settings.API_BASE_PATH}{settings.DOCS_URL}"
        )
        return ok_response(
            {
                "message": (
                    "This is the index endpoint of Africartes API. "
                    f"Its documentation is there: {docs_url}"
                )
            },
        )

    @router.get("/indicators")
    @cache_endpoint
    async def indicators(request: Request):
        """Return indicator list."""
        data_fs = request.app.state.data_fs
        indicator_info_repo = IndicatorInfoRepository.from_file(
            data_fs, INDICATORS_INFO_JSON
        )
        return ok_response(indicator_info_repo.content)

    @router.get("/indicators/area/{area_slug}/{indicator_slug}")
    @cache_endpoint
    async def indicators_for_area(
        request: Request,
        area_slug: AreaSlug,
        indicator_slug: str,
        remove_na: bool = True,
    ):
        """Return an indicator for each territory of an area."""
        data_fs = request.app.state.data_fs

        query_args = {
            "indicator_slug": indicator_slug,
            "area": area_slug,
        }

        csv_file = fs.path.combine("csv_data", f"{indicator_slug}_{area_slug}.csv")
        if not data_fs.exists(csv_file):
            return err_response(
                "Indicator data not found",
                args=query_args,
                status_code=status.HTTP_404_NOT_FOUND,
            )

        indicator_info_repo = IndicatorInfoRepository.from_file(
            data_fs, INDICATORS_INFO_JSON
        )

        csv_text = data_fs.readtext(csv_file)
        csv_io = StringIO(csv_text)
        result = build_indicators_for_area(
            area_slug, indicator_info_repo, indicator_slug, csv_io, remove_na
        )

        return ok_response(result, args=query_args)

    @router.get("/indicators/territories")
    @cache_endpoint
    async def indicators_by_territories(
        request: Request,
        territory_codes: List[str] = Query(..., alias="code"),
    ):
        """Get all indicators by territory, accepting many territories.

        Each territory code can be either:
        - a continent code: only AF is supported
        - a country ISO alpha-3 code
        - a DR (direction régionale) code

        Note: continent codes come from
        https://datahub.io/core/continent-codes#resource-continent-codes
        """
        data_fs = request.app.state.data_fs

        query_args = {"territory_codes": territory_codes}

        territory_info_json = load_json_file(data_fs, "territory_info.json")
        territories_model = model.Territories.parse_obj(territory_info_json)

        indicators_df = pd.DataFrame()

        for territory_code in territory_codes:
            try:
                territory_type = territories_model.get_territory_type(territory_code)
            except ValueError as exc:
                return err_response(
                    exc, args=query_args, status_code=status.HTTP_404_NOT_FOUND
                )

            json_file = fs.path.combine(
                f"{territory_type.lower()}_data", f"{territory_code}_indicators.json"
            )
            if not data_fs.exists(json_file):
                return err_response(
                    f"Data not found for territory code {territory_code!r}",
                    args=query_args,
                    status_code=status.HTTP_404_NOT_FOUND,
                )

            territory_indicators = load_json_file(data_fs, json_file)
            for indicator_slug, observations in territory_indicators[
                "indicators"
            ].items():  # noqa
                indicator_territory_df = pd.DataFrame(
                    observations, columns=["period", "value"]
                )
                indicator_territory_df["indicator_slug"] = indicator_slug
                indicator_territory_df["territory_code"] = territory_code
                indicators_df = pd.concat([indicators_df, indicator_territory_df])

        indicators_by_slug = {
            indicator_slug: {
                territory_code: sub_df2.drop("territory_code", axis=1).to_dict(
                    orient="list"
                )
                for territory_code, sub_df2 in sub_df.drop(
                    "indicator_slug", axis=1
                ).groupby("territory_code")
            }
            for indicator_slug, sub_df in indicators_df.groupby("indicator_slug")
        }

        return ok_response(indicators_by_slug, args=query_args)

    @router.get("/population_by_country/{country_code}")
    @cache_endpoint
    async def population_by_country(
        request: Request,
        country_code: str = Query(..., min_length=3, max_length=3, regex=r"^[A-Z]{3}$"),
    ):
        """Get population info by country iso alpha-3 code (for african countries only)."""  # noqa
        data_fs = request.app.state.data_fs

        query_args = {
            "country_code": country_code,
        }

        csv_file = fs.path.combine("pop_data", f"pop_{country_code}.csv")
        if not data_fs.exists(csv_file):
            return err_response(
                f"Unknown country code [{country_code}]",
                args=query_args,
                status_code=status.HTTP_404_NOT_FOUND,
            )

        csv_text = data_fs.readtext(csv_file)
        csv_io = StringIO(csv_text)
        result = build_population_data(country_code, csv_io)
        return ok_response(result, args=query_args)

    @router.get("/territories")
    @cache_endpoint
    async def territories(request: Request):
        """Load metadata about territories (countries, DR)."""
        data_fs = request.app.state.data_fs
        territory_info_json = load_json_file(data_fs, "territory_info.json")
        return ok_response(territory_info_json)

    @router.get("/topologies")
    @cache_endpoint
    async def topologies(request: Request):
        """Return a list of available GeoJSON topologies."""
        data_fs = request.app.state.data_fs
        topologies = sorted(
            [
                {"slug": Path(filename).stem}
                for filename in data_fs.listdir("topologies")
                if filename.endswith(".geojson")
            ],
            key=lambda d: d["slug"],
        )
        return ok_response(topologies)

    @router.get("/topologies/{area_slug}")
    @cache_endpoint
    async def topology(request: Request, area_slug: AreaSlug):
        """Return a GeoJSON topology."""
        data_fs = request.app.state.data_fs
        query_args = {"area_slug": area_slug}
        topology_file = fs.path.combine("topologies", f"{area_slug}.geojson")
        topology = load_json_file(data_fs, topology_file)
        return ok_response(topology, args=query_args)

    return router
