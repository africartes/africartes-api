"""Used by Docker base image.

Cf https://github.com/tiangolo/uvicorn-gunicorn-docker#how-to-use
"""

from toutafrique_api import create_app

app = create_app()
