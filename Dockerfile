FROM tiangolo/uvicorn-gunicorn:python3.8-slim-2020-04-27

ARG SETUPTOOLS_SCM_PRETEND_VERSION

RUN apt-get update && \
    apt-get install --yes --no-install-recommends git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN rm /app/prestart.sh

ENV PIP_NO_CACHE_DIR 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install sentry-sdk==0.14.4

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .
RUN pip install -e .
