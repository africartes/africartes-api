# toutafrique-api: Africartes Web API
# By: Jailbreak Team <team@jailbreak.paris>
#
# Copyright (C) 2020 Jailbreak
# https://gitlab.com/africartes/africartes-api
#
# toutafrique-api is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# toutafrique-api is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""Model definition."""

from dataclasses import dataclass
from typing import Any, Dict, List, Literal, Optional

from fs.base import FS
from pydantic import BaseModel

from .filesystem import load_json_file

TerritoryCode = str
TerritoryType = Literal["continent", "country", "DR"]


class Areas(BaseModel):
    africa: List[TerritoryCode]
    africa_dr: List[TerritoryCode]
    world: List[TerritoryCode]


class Territory(BaseModel):
    code: TerritoryCode
    name: str
    type: TerritoryType


class Territories(BaseModel):
    areas: Areas
    territories: List[Territory]

    def get_territory_type(self, territory_code: TerritoryCode) -> TerritoryType:
        from .util import find  # avoid cyclic imports

        try:
            territory = find(
                lambda territory: territory.code == territory_code, self.territories
            )
        except ValueError as exc:
            raise ValueError(f"Territory code {territory_code!r} not found") from exc
        return territory.type


@dataclass
class IndicatorInfoRepository:
    """Read and access indicator metadata.

    Index indicators by slug.
    """

    content: dict

    def __post_init__(self):
        self._index = {
            indicator["slug"]: indicator for indicator in self.content["indicators"]
        }

    @classmethod
    def from_file(cls, fs: FS, filename: str):
        """Initialize from JSON file (e.g. indicators_info.json)."""
        return IndicatorInfoRepository(content=load_json_file(fs, filename))

    def find_by_indicator_slug(self, indicator_slug: str) -> Optional[Dict[str, Any]]:
        """Find indicator info by slug."""
        return self._index.get(indicator_slug)
