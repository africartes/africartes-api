# toutafrique-api: Africartes Web API
# By: Jailbreak Team <team@jailbreak.paris>
#
# Copyright (C) 2020 Jailbreak
# https://gitlab.com/africartes/africartes-api
#
# toutafrique-api is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# toutafrique-api is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""All functions called or used by routes code."""


import decimal
import re
from io import IOBase
from typing import Any, Callable, Dict, Optional, Sequence, Tuple, TypeVar

import pandas as pd
from fastapi.responses import JSONResponse

from .model import IndicatorInfoRepository
from .version import __version__

NA = "NA"
YEAR = "Year"

RANGE_RE = re.compile(r"^Y(\d+)T(\d+)$")
GE_RE = re.compile(r"^Y_GE(\d+)$")
LT_RE = re.compile(r"^Y_LT(\d+)$")

INDICATOR_DATA_KEYS = {
    "slug",
    "name",
    "long_name",
    "description",
    "legend",
    "na_color",
    "source",
    "unit",
}


# TODO remove this function
def build_response_functions() -> Tuple[Callable, Callable]:
    """Create and return ok_response and err_response function from API version info."""

    def common_response(args: Dict[str, Any] = {}):
        """Produce common information return by all endpoints."""
        return {"_meta": {"api_version": __version__, "args": args}}

    def ok_response(result: Any, args: Dict[str, Any] = {}):
        """Successful response."""
        return {
            **common_response(args),
            "success": True,
            "result": result,
        }

    def err_response(
        error_content: Any,
        args: Dict[str, Any] = {},
        status_code: int = 500,
    ):
        """Error response."""
        return JSONResponse(
            content={
                **common_response(args),
                "success": False,
                "error": str(error_content),
            },
            status_code=status_code,
        )

    return ok_response, err_response


def iter_records_without_none_values(observations):
    """Iterate on records to remove None (empty) values."""
    for record in observations:
        non_na_values = {k: v for k, v in record.items() if k != YEAR and v}
        if non_na_values:
            yield {YEAR: record[YEAR], **non_na_values}


def value_to_decimal(value):
    """Convert string value to decimal.Decimal object."""
    return None if pd.isna(value) else decimal.Decimal(value)


def build_indicators_for_area(
    area_slug: str,
    indicator_info_repo: IndicatorInfoRepository,
    indicator_slug: str,
    csv_file: IOBase,
    remove_na: bool,
):
    """Build indicator from CSV file content."""
    # First force read as string
    df = pd.read_csv(csv_file, dtype=str)

    # type data based on column
    for col in df.columns:
        if col == "Year":
            df[col] = df[col].apply(int)
        else:
            # use python decimal to insure float precision
            df[col] = df[col].apply(value_to_decimal)

    observations = df.to_dict("records")
    if remove_na:
        observations = list(iter_records_without_none_values(observations))
    indicator_info = indicator_info_repo.find_by_indicator_slug(indicator_slug)

    # Set brackets key from country_brackets or dr_brackets according to area_slug.
    scale_info = indicator_info["scale"]

    return {
        **pick(indicator_info, INDICATOR_DATA_KEYS),
        "observations": observations,
        "scale": {
            **pick(scale_info, {"type"}),
            "brackets": scale_info["dr_brackets"]
            if area_slug == "africa_dr" and "dr_brackets" in scale_info
            else scale_info["country_brackets"],
        },
    }


def compute_age_range_label(age_range_code: str) -> str:
    """Create age range label from age range code.

    >>> compute_age_range_label('Y30T34')
    '30-34'
    >>> compute_age_range_label('Y80T84')
    '80-84'
    >>> compute_age_range_label('Y_GE100')
    '100+'
    >>> compute_age_range_label('Y_LT5')
    '< 5'
    >>> compute_age_range_label('_T')
    'total'
    """
    if age_range_code == "_T":
        return "total"

    m = RANGE_RE.match(age_range_code)
    if m:
        return f"{m.group(1)}-{m.group(2)}"

    m = GE_RE.match(age_range_code)
    if m:
        return f"{m.group(1)}+"

    m = LT_RE.match(age_range_code)
    if m:
        return f"< {m.group(1)}"

    raise ValueError("Unknown age range code: [%s]", age_range_code)


def compute_sortable_age_range_code(age_range_code):
    """Create age range sortable code from age range code.

    >>> compute_sortable_age_range_code('Y30T34')
    'Y030-034'
    >>> compute_sortable_age_range_code('Y80T84')
    'Y080-084'
    >>> compute_sortable_age_range_code('Y_GE100')
    'Y100'
    >>> compute_sortable_age_range_code('Y_LT5')
    'Y000-005'
    >>> compute_sortable_age_range_code('_T')
    'TOTAL'
    """
    if age_range_code == "_T":
        return "TOTAL"

    def pad(value_str: str) -> str:
        return value_str.zfill(3)

    m = RANGE_RE.match(age_range_code)
    if m:
        return f"Y{pad(m.group(1))}-{pad(m.group(2))}"

    m = GE_RE.match(age_range_code)
    if m:
        return f"Y{pad(m.group(1))}"

    m = LT_RE.match(age_range_code)
    if m:
        return f"Y000-{pad(m.group(1))}"

    raise ValueError("Unknown age range code: [%s]", age_range_code)


def build_population_data(country_code: str, csv_file: IOBase):
    """Build population data."""
    df = pd.read_csv(csv_file)
    df = df.sort_values(by=["year"])

    # Organize data
    observations = {}
    for year, row in df.groupby(by=["year"]):

        sex_age_value_it = zip(
            row["sex"].tolist(),
            row["age"].tolist(),
            row["value"].fillna(NA).tolist(),
        )
        observations[year] = [
            (sex, age, value)
            for sex, age, value in sex_age_value_it
            # TODO: age range values such as _T or _Z are diltered out here.
            # It'll be relevant to use this filter on DBnomics API call to avoid
            # to download useless data
            # For this, we had to find out a way to call API with
            # - either age != (_T and _Z)
            # - or get all possible values for age and filter on these values except
            # those that don't start with 'Y'
            if value != NA and age.startswith("Y")
        ]

    legend = {
        "age": [
            (age_code, compute_age_range_label(age_code))
            for age_code in sorted(
                df["age"].unique().tolist(),
                key=compute_sortable_age_range_code,
            )
        ],
        "sex": {"M": "homme", "F": "femme"},
    }

    return {
        "country": country_code,
        "legend": legend,
        "observations": observations,
    }


class Raise:
    pass


T = TypeVar("T")


def find(predicate, items: Sequence[T], default=Raise) -> Optional[T]:
    item = next(filter(predicate, items), None)
    if item is None:
        if default is Raise:
            raise ValueError("Item not found")
        return default
    return item


def pick(mapping, keys):
    """Restrict mapping key, values to given keys."""
    return {k: v for k, v in mapping.items() if k in keys}
