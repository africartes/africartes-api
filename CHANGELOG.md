# Changelog

## 2.0.3

Non-breaking changes:

- Add optional cache for S3 reads and endpoints using Redis ([!7](https://gitlab.com/africartes/africartes-api/-/merge_requests/7))

## 2.0.2

Non-breaking changes:

- Add a TTL cache to files read from PyFilesystem via `DATA_FS_URL` to improve performance.

## 2.0.1

- Do not preset DATA_FS_URL variable in Docker image

## 2.0.0

Breaking changes:

- Add support for Africa continent data aggregates
- Merge `indicators_by_country`, `indicators_by_dr` into `indicators_by_territories`, allow passing multiple territory codes
- Do not return geographic zones that have no data
- Fix a bug with floating-point precision
- Return metadata in `indicators` endpoint, and no metadata in other endpoints

## 0.2.1

- Fixup: move `na_color` back to indicator, because `indicators_for_area` endpoint does not need `scale`, but needs `na_color`.

## 0.2.0

Breaking changes:

- Serve scale under a unique `scale` property for each indicator. Related to [this commit](https://framagit.org/jailbreak/toutafrique/toutafrique-data-processing/-/commit/b70f0e9883b3543cc36dccb5d579313d1e226c3e) in toutafrique-data-processing.

## 0.1.8

- Add [`GZipMiddleware`](https://fastapi.tiangolo.com/advanced/middleware/#gzipmiddleware)

## 0.1.7

- `population_by_country` endpoint: serve age legend as ordered list of pairs

## 0.1.6

- `population_by_country` endpoint: more compact response

## 0.1.5

- `population_by_country` endpoint:
  - enhance age range labels
  - sort age range labels
- refactor util.py to remove dependency to **init**.py

## 0.1.4

- fix `population_by_country` endpoint
