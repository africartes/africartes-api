import pytest
import redis.exceptions
from fastapi.testclient import TestClient
from redis import Redis

from toutafrique_api import create_app
from toutafrique_api.config import Settings


@pytest.fixture
def settings():
    return Settings(
        _env_file=None,
        DATA_FS_URL="./tests/end-to-end/fixtures/",
    )


@pytest.fixture
def settings_with_api_base_path(settings):
    settings2 = settings.copy()
    settings2.API_BASE_PATH = "/foo"
    return settings2


@pytest.fixture
def settings_with_redis_url(settings):
    settings2 = settings.copy()
    settings2.REDIS_URL = "redis://localhost:6379"
    return settings2


@pytest.fixture
def client(settings):
    with TestClient(create_app(settings)) as client:
        yield client


@pytest.fixture
def client_with_api_base_path(settings_with_api_base_path):
    with TestClient(create_app(settings_with_api_base_path)) as client:
        yield client


@pytest.fixture
def client_with_redis(settings_with_redis_url):
    with TestClient(create_app(settings_with_redis_url)) as client:
        yield client


@pytest.fixture(scope="session")
def redis_service(docker_ip, docker_services):
    """Ensure that Redis service is up and responsive."""

    def is_responsive(port):
        try:
            redis_client.ping()
        except redis.exceptions.ConnectionError:
            return False
        return True

    # `port_for` takes a container port and returns the corresponding host port
    port = docker_services.port_for("redis", 6379)
    redis_client = Redis(host=docker_ip, port=port)
    docker_services.wait_until_responsive(
        timeout=30.0, pause=0.1, check=lambda: is_responsive(port)
    )
    return redis_client
