from typing import Generic, Protocol, TypeVar

T = TypeVar("T", bytes, float, int, str)


class Cache(Protocol, Generic[T]):
    def get(self, key: str) -> T:
        pass

    def set(self, key: str, value: T):
        pass
